package bumperdb

import (
	"time"
)

type Block struct {
	Id               uint64    `db:"id"`
	UserId           uint64    `db:"from_user"`
	BlockedUserId    uint64    `db:"blocked"`
	DatetimeRecorded time.Time `db:"recorded"`
}

func (db *BumperDB) IsBlocked(isUserID, blockedByUserID uint64) (isBlocked bool) {
	// Is the first userID blocked by the secondUserId
	return
}
