package bumperdb

import (
	"log"
	"time"
)

type User struct {
	Id         uint64    `json:"id"`
	FirstName  string    `json:"firstName" db:"first_name"`
	LastName   string    `json:"lastName" db:"last_name"`
	Age        uint8     `json:"age" db:"age"`
	Gender     string    `json:"gender" db:"gender"`
	LastActive time.Time `json:"lastActive" db:"last_active"`
}

func (u *User) IsBlockedBy(db *BumperDB, user_id uint64) bool {
	return db.IsBlocked(u.Id, user_id)
}

func (u *User) LastKnownLocation(db *BumperDB) Location {
	return db.LastKnownLocation(u.Id)
}

type UserProfile struct {
	Id       uint64
	UserId   uint64 `db:"user_id"`
	About    string `db:"about"`
	JobTitle string `db:"job_title"`
	// URLs to 5 cropped profile pictures
	ProfilePictures [5]string `json:"pics"`
}

type SearchPreferences struct {
	Id                uint64
	UserId            uint64 `json:"id" db:"user_id"`
	InterestedInMen   bool   `json:"men" db:"men"`
	InterestedInWomen bool   `json:"women" db:"women"`
	MinAge            uint8  `json:"minAge" db:"min_age"`
	MaxAge            uint8  `json:"maxAge" db:"max_age"`
}

type CompleteUserProfileView struct {
	Id         uint64    `json:"id"`
	FirstName  string    `json:"firstName" db:"first_name"`
	LastName   string    `json:"lastName" db:"last_name"`
	Age        uint8     `json:"age" db:"age"`
	Gender     string    `json:"gender" db:"gender"`
	LastActive time.Time `json:"lastActive" db:"last_active"`
	About      string    `db:"about"`
	JobTitle   string    `db:"job_title"`
	// URLs to 5 cropped profile pictures
	ProfilePictures [5]string `json:"pics"`
}

// Return the last known location for a UserProfile
func (d *BumperDB) LastKnownLocation(u interface{}) (point Location) {
	var userID uint64
	switch u.(type) {
	case UserProfile:
		user := u.(UserProfile)
		userID = user.Id
	case uint64:
		userID = u.(uint64)
	default:
		log.Println("BumperDB.LastKnownLocation: Unable to lookup point for non UserProfile or UserID")
		return
	}
	log.Println(userID)
	return
}

type UserAnalytics struct {
	UserID          string  `json:"id"`
	TotalBumps      uint16  `json:"bumps"`
	TotalKmTraveled float32 `json:"distance"`
	TotalLikes      uint16  `json:"likes"`
}
