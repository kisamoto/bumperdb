package bumperdb

import (
	"time"
)

type Location struct {
	Id               int64     `db:"id"`
	UserId           int64     `json:"-,omitempty" msgpack:"owner,omitempty" db:"user_id"`
	DatetimeRecorded time.Time `json:"datetimeRecorded,omitempty" msgpack:"datetimeRecorded,omitempty" db:"recorded"`
	Latitude         float64   `json:"lat" msgpack:"lat" db:"latitude"`
	Longitude        float64   `json:"lon" msgpack:"lon" db:"longitude"`
}

type Visit struct {
	Id               int64     `db:"id"`
	UserId           int64     `db:"user_id"`
	DatetimeRecorded time.Time `db:"recorded"`
	Latitude         float64   `db:"latitude"`
	Longitude        float64   `db:"longitude"`
	Duration         int64     `db:"duration"`
}

type Place struct {
	Id               int64     `db:"id"`
	DatetimeRecorded time.Time `db:"recorded"`
	Latitude         float64   `db:"latitude"`
	Longitude        float64   `db:"longitude"`
}

type PlaceVisits struct {
	Id      int64 `db:"id"`
	PlaceId int64 `db:"place_id"`
	VisitId int64 `db:"visit_id"`
}

type PlaceProfile struct {
	Id          int64  `db:"id"`
	PlaceId     int64  `db:"place_id"`
	Title       string `db:"title"`
	Description string `db:"description"`
}

type Distance struct {
	Id     int64     `db:"id"`
	UserId int64     `db:"user_id"`
	Day    time.Time `db:"day"`
	Km     int64     `db:"km"`
}
