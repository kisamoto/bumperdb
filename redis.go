package bumperdb

import (
	"fmt"
	"github.com/garyburd/redigo/redis"
	"github.com/vmihailenco/msgpack"
	"log"
)

func ProcessingQueueName(queue string) string {
	return fmt.Sprintf("%v%v", queue, processQ)
}

/*
Queue Lengths
*/
func (b *BumperDB) QueueLength(queue string) (len int64, err error) {
	conn := b.redisQPool.Get()
	defer conn.Close()
	return redis.Int64(conn.Do(rLen, queue))
}

func (b *BumperDB) MsgQueueLength() (len int64, err error) {
	return b.QueueLength(b.rMsgList)
}

func (b *BumperDB) LocQueueLength() (len int64, err error) {
	return b.QueueLength(b.rLocList)
}

// Processing queue lengths
func (b *BumperDB) ProcessingQueueLength(queue string) (len int64, err error) {
	return b.QueueLength(ProcessingQueueName(queue))
}

func (b *BumperDB) MsgProcessingQueueLength() (len int64, err error) {
	return b.ProcessingQueueLength(b.rMsgList)
}

func (b *BumperDB) LocProcessingQueueLength() (len int64, err error) {
	return b.ProcessingQueueLength(b.rLocList)
}

/*
Pushers
*/
func (b *BumperDB) Push(i interface{}, queue string) (len int64, err error) {
	conn := b.redisQPool.Get()
	defer conn.Close()
	return redis.Int64(conn.Do(rPush, queue, Encode(i)))
}

func (b *BumperDB) MsgPush(m Message) (len int64, err error) {
	return b.Push(m, b.rMsgList)
}

func (b *BumperDB) LocPush(p Location) (len int64, err error) {
	return b.Push(p, b.rLocList)
}

/*
Poppers
*/
func (b *BumperDB) PopPushProcessing(queue string) (msg []byte, err error) {
	conn := b.redisQPool.Get()
	defer conn.Close()
	return redis.Bytes(conn.Do(rPopPush, queue, ProcessingQueueName(queue)))
}

func (b *BumperDB) MsgPopPushProcessing() (msg Message, err error) {
	msgB, err := b.PopPushProcessing(b.rMsgList)
	if err == nil {
		err = msgpack.Unmarshal(msgB, &msg)
	}
	return
}

func (b *BumperDB) LocPopPushProcessing() (loc Location, err error) {
	locB, err := b.PopPushProcessing(b.rLocList)
	if err == nil {
		err = msgpack.Unmarshal(locB, &loc)
	}
	return
}

/*
Removers
*/
func (b *BumperDB) Remove(message interface{}, queue string) (removed int64, err error) {
	conn := b.redisQPool.Get()
	defer conn.Close()
	removed, err = redis.Int64(conn.Do(rLRem, queue, 1, Encode(message)))
	return
}

// Shortcut to remove a message from the equivalent redis processing queue.
func (b *BumperDB) RemoveProcessing(message interface{}, queue string) (removed int64, err error) {
	return b.Remove(message, ProcessingQueueName(queue))
}

func (b *BumperDB) RemoveMsgProcessing(message Message) (removed int64, err error) {
	return b.RemoveProcessing(message, b.rMsgList)
}

func (b *BumperDB) RemoveLocProcessing(message Location) (removed int64, err error) {
	return b.RemoveProcessing(message, b.rLocList)
}

/*
Requeuers
*/
func (b *BumperDB) RemoveProcessingRequeue(message interface{}, queue string) (err error) {
	_, err = b.Push(message, queue)
	if err != nil {
		return
	}
	_, err = b.RemoveProcessing(message, queue)
	return
}

func (b *BumperDB) MsgRemoveProcessingRequeue(m Message) (err error) {
	return b.RemoveProcessingRequeue(m, b.rMsgList)
}

func (b *BumperDB) LocRemoveProcessingRequeue(p Location) (err error) {
	return b.RemoveProcessingRequeue(p, b.rLocList)
}

func Encode(i interface{}) (b []byte) {
	// If already a []byte, just return
	switch i.(type) {
	case []byte:
		return i.([]byte)
	}
	b, err := msgpack.Marshal(i)
	PanicError(err)
	return
}

func HandleRedisError(err error) {
	if err != nil {
		switch err {
		case redis.ErrNil:
			// Just log and return. not fussed if the queue is empty
			log.Println(err)
			return
		default:
			PanicError(err)
		}
	}
}
