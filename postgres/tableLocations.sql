CREATE TABLE "user.info" (
  user_id BIGINT PRIMARY KEY,
  first_name VARCHAR(20) NOT NULL,
  last_name VARCHAR(20) NOT NULL,
  age SMALLINT,
  gender CHAR(1),
  joined DATE NOT NULL,
  last_active DATE NOT NULL,
  is_active BOOLEAN,
  enabled BOOLEAN
);

CREATE TABLE "user.profile" (
  user_id BIGINT REFERENCES "user.info",
  about VARCHAR(250),
  job_title VARCHAR(100)
);

CREATE TABLE "user.search_prefs" (
  user_id BIGINT REFERENCES "user.info",
  men BOOLEAN NOT NULL,
  women BOOLEAN NOT NULL,
  min_age SMALLINT NOT NULL,
  max_age SMALLINT NOT NULL
);

CREATE TABLE "user.locations" (
  user_id BIGINT REFERENCES "user.info",
  latitude FLOAT,
  longitude FLOAT,
  recorded DATE
);

CREATE TABLE "user.likes" (
  from_user BIGINT REFERENCES "user.info",
  likes BIGINT REFERENCES "user.info",
  recorded DATE
);

CREATE TABLE "user.bumps" (
  from_user BIGINT REFERENCES "user.info",
  to_user BIGINT REFERENCES "user.info",
  recorded DATE,
  latitude FLOAT,
  longitude FLOAT
);

CREATE TABLE "user.blocks" (
  from_user BIGINT REFERENCES "user.info",
  blocked BIGINT REFERENCES "user.info",
  recorded DATE
);

CREATE TABLE "user.reports" (
  from_user BIGINT REFERENCES "user.info",
  reports BIGINT REFERENCES "user.info",
  reason TEXT,
  recorded DATE
);

CREATE TABLE analytics.visits (
  user_id BIGINT REFERENCES "user.info",
  vist_id SERIAL PRIMARY KEY,
  recorded DATE,
  latitude FLOAT,
  longitude FLOAT,
  duration_mins INTEGER
);

CREATE TABLE analytics.places (
  place_id SERIAL PRIMARY KEY,
  recorded DATE,
  latitude FLOAT,
  longitude FLOAT
);

CREATE TABLE analytics.place_visits (
  place_id SERIAL REFERENCES analytics.places,
  visit_id SERIAL REFERENCES analytics.visits
);

CREATE TABLE analytics.place_profile(
  place_id SERIAL REFERENCES analytics.places,
  title VARCHAR(256),
  description TEXT
);

CREATE TABLE analytics.distances (
  user_id BIGINT REFERENCES "user.info",
  day DATE NOT NULL,
  distance_km INTEGER
);
