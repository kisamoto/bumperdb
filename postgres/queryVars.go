package postgres

import (
	"fmt"
)

func CreateSchema(schema string) string {
	return fmt.Sprintf(BaseCreateSchema, schema)
}

func CreateTable(schema, table, rows string) string {
	return fmt.Sprintf(BaseCreateTable, schema, table, rows)
}

func InsertInto(columns, schema, table, values string) string {
	return fmt.Sprintf(BaseInsert, columns, schema, table, values)
}

func SelectFrom(schema, table, columns, whereClause string) string {
	return fmt.Sprintf(BaseWhereSelect, columns, schema, table, whereClause)
}

var (
	BaseCreateSchema = "CREATE SCHEMA IF NOT EXISTS %v;"
	BaseCreateTable = "CREATE TABLE IF NOT EXISTS %v.%v (%v);"
	BaseInsert = "INSERT (%v) INTO %v.%v VALUES %v;"
	BaseSelect = "SELECT %v FROM %v.%v %v;"
	BaseWhereSelect = fmt.Sprint(BaseSelect, "%v", "%v", "%v", "WHERE %v")
)

var (
	InfoTableRows = `
		user_id BIGINT PRIMARY KEY,
		first_name VARCHAR(20) NOT NULL,
		last_name VARCHAR(20) NOT NULL,
		age SMALLINT,
		gender CHAR(1),
		joined DATE NOT NULL,
		last_active DATE NOT NULL,
		is_active BOOLEAN,
		enabled BOOLEAN`

	ProfileTableRows = `
		user_id BIGINT REFERENCES usr.info,
		about VARCHAR(250),
		job_title VARCHAR(100)`

	SearchPrefRows = `
		user_id BIGINT REFERENCES usr.info,
		men BOOLEAN NOT NULL,
		women BOOLEAN NOT NULL,
		min_age SMALLINT NOT NULL,
		max_age SMALLINT NOT NULL`

	LocationRows = `
		user_id BIGINT REFERENCES usr.info,
		latitude FLOAT,
		longitude FLOAT,
		recorded DATE`

	LikeRows = `
		from_user BIGINT REFERENCES usr.info,
		likes BIGINT REFERENCES usr.info,
		recorded DATE`

	BumpRows = `
		from_user BIGINT REFERENCES usr.info,
		to_user BIGINT REFERENCES usr.info,
		recorded DATE,
		latitude FLOAT,
		longitude FLOAT`

	BlockRows = `
		from_user BIGINT REFERENCES usr.info,
		blocked BIGINT REFERENCES usr.info,
		recorded DATE`

	ReportRows = `
		from_user BIGINT REFERENCES usr.info,
		reports BIGINT REFERENCES usr.info,
		reason TEXT,
		recorded DATE`
)

var (
	AnalyticsVisitsRows = `
		user_id BIGINT REFERENCES usr.info,
		vist_id SERIAL PRIMARY KEY,
		recorded DATE,
		latitude FLOAT,
		longitude FLOAT,
		duration_mins INTEGER`

	AnalyticsPlacesRows = `
		place_id SERIAL PRIMARY KEY,
		recorded DATE,
		latitude FLOAT,
		longitude FLOAT`

	AnalyticsPlaceVisitsRows = `
		place_id SERIAL REFERENCES analytics.places,
		visit_id SERIAL REFERENCES analytics.visits`

	AnalyticsPlaceProfileRows = `
		place_id SERIAL REFERENCES analytics.places,
		title VARCHAR(256),
		description TEXT`

	AnalyticsDistanceRows = `
		user_id BIGINT REFERENCES usr.info,
		day DATE NOT NULL,
		distance_km INTEGER`
)
