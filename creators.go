package bumperdb

import ()

// Creates a new user profile.
// Should really use an upsert so update on a userID
func (b *BumperDB) UpsertUser(u UserProfile) {
	// Try and find user
	// If present, update
	// If not, insert
}

// Creates new location points in PostgreSQL for analytics
func (b *BumperDB) CreateNewLocationPoint(p Location) {
	validator := LocationValidator{db: b}
	isValid, err := validator.IsValid(p)
	PanicError(err)
	if isValid {
		b.AddLocationPoint(p)
	}
}

// Create a new directed like between two users
func (b *BumperDB) CreateNewLike(l Like) {

}
