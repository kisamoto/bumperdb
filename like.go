package bumperdb

import (
	"time"
)

type Like struct {
	Id            uint64    `db:"id"`
	UserId        uint64    `db:"from_user"`
	LikesUserId   uint64    `db:"likes"`
	DateTimeLiked time.Time `db:"recorded"`
}

func (db *BumperDB) Liked(l Like) {
	// Update PostgreSQL with like info
	return
}

func (db *BumperDB) HasLiked(userID1, userID2 uint64) (hasLiked bool) {
	return
}

func (db *BumperDB) HaveLikedEachOther(userID1, userID2 uint64) (haveLikedEachOther bool) {
	// Save a db call, check User1 -> User2 && User2 -> User1
	return db.HasLiked(userID1, userID2) && db.HasLiked(userID2, userID1)
}
