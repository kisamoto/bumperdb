package bumperdb

import (
	"fmt"
)

type Validator interface {
	IsValid(interface{}) (bool, error)
}

type LocationValidator struct {
	db *BumperDB
}

func (v *LocationValidator) IsValid(p Location) (valid bool, err error) {
	// Check distance compared to previous point
	// Check speed is below threshold
	// Check max distance
	return
}

type LikeValidator struct {
	db *BumperDB
}

func (v *LikeValidator) IsValid(l Like) (valid bool, err error) {
	// Check users have actually bumped into each other
	haveBumpedIntoEachOther, _ := v.db.HasBumpedInto(l.UserId, l.LikesUserId)
	if !haveBumpedIntoEachOther {
		return false, fmt.Errorf(errNotBumped, l.UserId, l.LikesUserId)
	}
	return haveBumpedIntoEachOther, nil
}

type MessageValidator struct {
	db *BumperDB
}

func (v *MessageValidator) IsValid(m Message) (valid bool, err error) {
	// Check users have actually bumped into each other
	haveBumpedIntoEachOther, _ := v.db.HasBumpedInto(m.FromUserId, m.ToUserId)
	if !haveBumpedIntoEachOther {
		return false, fmt.Errorf(errNotBumped, m.FromUserId, m.ToUserId)
	}
	// Check users have liked each other
	usersHaveLikedEachOther := v.db.HaveLikedEachOther(m.FromUserId, m.ToUserId)
	if !usersHaveLikedEachOther {
		return false, fmt.Errorf(errNotLikedEachOther, m.FromUserId, m.ToUserId)
	}
	// Check user is not blocked
	userANotBlockedByB := v.db.IsBlocked(m.FromUserId, m.ToUserId)
	if !userANotBlockedByB {
		return false, fmt.Errorf(errBlocked, m.FromUserId, m.ToUserId)
	}
	return true, nil
}

type BlockValidator struct {
	db *BumperDB
}

func (v *BlockValidator) IsValid(b Block) (valid bool, err error) {
	// Check users have actually bumped into each other
	haveBumpedIntoEachOther, _ := v.db.HasBumpedInto(b.UserId, b.BlockedUserId)
	if !haveBumpedIntoEachOther {
		return false, fmt.Errorf(errNotBumped, b.UserId, b.BlockedUserId)
	}
	return true, nil
}
