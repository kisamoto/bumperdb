package bumperdb

import (
	"database/sql"
	"github.com/coopernurse/gorp"
	"github.com/garyburd/redigo/redis"
	_ "github.com/lib/pq"
	"log"
	"time"
)

// The base struct to be used in the Bumper API/Workers/Analytics
// Provides access to the redis queue and postgreSQL databases
type BumperDB struct {
	redisQPool *redis.Pool
	postgresDb *sql.DB
	dbmap      *gorp.DbMap

	// Redis settings
	// Queue for
	// Using RPOPLPUSH onto a processing queue
	rLocList string
	rMsgList string

	// Postgres settings
	pUserSchema      string
	pAnalyticsSchema string
}

func NewBumperDB() (db *BumperDB) {
	db = &BumperDB{}
	return
}

func (d *BumperDB) SetRedis(network, address, locQueue, msgQueue string) {
	d.redisQPool = newRedisPool(network, address)
	d.rLocList = locQueue
	d.rMsgList = msgQueue
}

func (d *BumperDB) SetPostgres(postgresURI string) {
	db, err := sql.Open("postgres", postgresURI)
	PanicError(err)
	d.postgresDb = db
}

func (d *BumperDB) SetPostgresSchema(user, analytics string) {
	d.pUserSchema = user
	d.pAnalyticsSchema = analytics
}

func (d *BumperDB) SetupPostgres(postgresURI, userSchema, analyticsSchema string) {
	d.SetPostgres(postgresURI)
	d.SetPostgresSchema(userSchema, analyticsSchema)
	d.initDB()
	d.dbmap.CreateTablesIfNotExists()
}

func (d *BumperDB) initDB() {
	var table *gorp.TableMap
	dbmap := &gorp.DbMap{Db: d.postgresDb, Dialect: gorp.PostgresDialect{}}

	table = dbmap.AddTableWithNameAndSchema(User{}, d.pUserSchema, tableUser).SetKeys(true, "Id")
	table.ColMap("first_name").SetMaxSize(50)
	table.ColMap("last_name").SetMaxSize(50)

	table = dbmap.AddTableWithNameAndSchema(UserProfile{}, d.pUserSchema, tableUserProfile).SetKeys(true, "Id")
	table.ColMap("user_id").SetUnique(true)
	table.ColMap("job_title").SetMaxSize(100)

	table = dbmap.AddTableWithNameAndSchema(SearchPreferences{}, d.pUserSchema, tableSearchPreferences).SetKeys(true, "Id")
	table.ColMap("user_id").SetUnique(true)

	dbmap.AddTableWithNameAndSchema(Location{}, d.pUserSchema, tableLocations).SetKeys(true, "Id")
	dbmap.AddTableWithNameAndSchema(Like{}, d.pUserSchema, tableLikes).SetKeys(true, "Id")
	dbmap.AddTableWithNameAndSchema(Bump{}, d.pUserSchema, tableBumps).SetKeys(true, "Id")
	dbmap.AddTableWithNameAndSchema(Block{}, d.pUserSchema, tableBlocks).SetKeys(true, "Id")
	dbmap.AddTableWithNameAndSchema(Report{}, d.pUserSchema, tableReports).SetKeys(true, "Id")

	dbmap.AddTableWithNameAndSchema(Visit{}, d.pAnalyticsSchema, tableVisits).SetKeys(true, "Id")
	dbmap.AddTableWithNameAndSchema(Place{}, d.pAnalyticsSchema, tablePlaces).SetKeys(true, "Id")
	dbmap.AddTableWithNameAndSchema(PlaceVisits{}, d.pAnalyticsSchema, tablePlaceVisits).SetKeys(true, "Id")
	dbmap.AddTableWithNameAndSchema(PlaceProfile{}, d.pAnalyticsSchema, tablePlaceProfile).SetKeys(true, "Id")
	dbmap.AddTableWithNameAndSchema(Distance{}, d.pAnalyticsSchema, tableDistances).SetKeys(true, "Id")

	d.dbmap = dbmap
}

func newRedisPool(network, address string) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     3,
		IdleTimeout: 240 * time.Second,
		Dial: func() (c redis.Conn, err error) {
			c, err = redis.Dial(network, address)
			if err != nil {
				return nil, err
			}
			return
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
}

func PanicError(err error) {
	if err != nil {
		log.Panic(err)
	}
}
