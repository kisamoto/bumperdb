package bumperdb

import (
	"time"
)

// A bump occurs between 2 users at a location (or multiple locations)
// It does not matter what order the Users are provided
type Bump struct {
	Id               uint64    `json:"-" db:"id"`
	UserId           uint64    `json:"from_user" db:"from_user"`
	ToUserId         uint64    `json:"to_user" db:"to_user"`
	DatetimeRecorded time.Time `json:"recorded" db:"recorded"`
	Latitude         float64   `json:"lat" db:"latitude"`
	Longitude        float64   `json:"lon" db:"longitude"`
}

func NewBump(u1, u2 UserProfile, loc Location) *Bump {
	return &Bump{
		UserId:           u1.Id,
		ToUserId:         u2.Id,
		DatetimeRecorded: loc.DatetimeRecorded,
		Latitude:         loc.Latitude,
		Longitude:        loc.Longitude}
}

// Returns a boolean if the user has bumped into a second user and a list of the locations it happened.
func (db *BumperDB) HasBumpedInto(userID1, userID2 uint64) (hasBumpedInto bool, locations []Location) {
	// Todo: Must combine locations of to/from vertices
	return
}

func (db *BumperDB) BumpedInto(b Bump) {
	// Update PostgreSQL with Bump info
	// Check if they have already bumped into each other, and if so update vertice with new Location.
	return
}

func (db *BumperDB) AllBumps(u UserProfile) (bumps []Bump) {
	// Fetch all bumps (incoming and outgoing vertices) from PostgreSQL that match bump preferences
	// Filter out users which don't meet their bump preferences
	// E.g. AllBumps for a male UserProfile should remove all UserProfile with InterestedInMen == false
	// Todo: Must combine locations of to/from vertices
	return
}
