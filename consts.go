package bumperdb

// Rate limit validation
const (
	maxReqPerS = 5
)

// Transport
const (
	maxSpeedMs = 500
	// Circumference of world
	maxDistancePerDayM = 40008 * 1000
)

// Redis command consts
const (
	rPush    = "LPUSH"
	rPopPush = "RPOPLPUSH"
	rLRem    = "LREM"
	rLen     = "LLEN"
	// After longer than this on the processing queue should
	// then be requeued to try again.
	maxTimeOnProcessingQueueSeconds = 10
	delaySeconds                    = 1
)

const (
	processQ = "Processing"
)

// PostgreSQL consts
const (
	userTablePrefix      = "user_"
	analyticsTablePrefix = "analytics_"

	// User stuff
	tableUser              = "info"
	tableUserProfile       = "profile"
	tableSearchPreferences = "search_prefs"
	tableLocations         = "locations"

	// Analytics
	tableVisits       = "visits"
	tablePlaces       = "places"
	tablePlaceVisits  = "place_visits"
	tablePlaceProfile = "place_profile"
	tableDistances    = "distances"

	// On their own
	tableLikes   = "likes"
	tableBumps   = "bumps"
	tableBlocks  = "blocks"
	tableReports = "reports"
)
