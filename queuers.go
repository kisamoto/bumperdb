package bumperdb

import ()

// Shortcut Queuers that will only add to queue if provided point valid.
func (b *BumperDB) QueueLocationPoint(p Location) {
	v := LocationValidator{db: b}
	valid, err := v.IsValid(p)
	PanicError(err)
	if valid {
		_, err := b.LocPush(p)
		PanicError(err)
	}
}

func (b *BumperDB) QueueMessage(m Message) {
	v := MessageValidator{db: b}
	valid, err := v.IsValid(m)
	PanicError(err)
	if valid {
		_, err := b.MsgPush(m)
		PanicError(err)
	}
}
