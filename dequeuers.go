package bumperdb

import (
	"fmt"
	_ "time"
)

/*
This method could be over complicated (just return the status or err from a Create method)
but allows us to check the length of time on the "processing" queue and handle
accordingly.
//*/
//func (b *BumperDB) DeQueue() {
//	// Use a channel to
//	var c chan bool = make(chan bool, 1)
//	secondsProcessingQueue := 0
//	message := b.PopPushLocation()
//	mp := &message
//	// ToDo: Due to msgpack must always convert to UTC
//	mp.DatetimeRecorded = mp.DatetimeRecorded.UTC()
//	// Now listen to see if we failed or not
//	PrintMessage(message, c)
//	for {
//		switch {
//		case <-c:
//			// Successfully created/upsert, then remove from processing queue
//			b.RemoveProcessing(message)
//			return
//		case false == <-c:
//			// Failed to add to database, remove from processing and re-add to queue for later
//			b.RemoveProcessingRequeue(message)
//			return
//		case secondsProcessingQueue >= maxTimeOnProcessingQueueSeconds:
//			// Was too long on the processing queue, removed and requeued
//			b.RemoveProcessingRequeue(message)
//			return
//		case secondsProcessingQueue < maxTimeOnProcessingQueueSeconds:
//			time.Sleep(delaySeconds * time.Second)
//			secondsProcessingQueue += delaySeconds
//		}
//	}
//}

func PrintMessage(m interface{}, c chan bool) {
	fmt.Println(m)
	c <- true
}
