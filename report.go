package bumperdb

import (
	"time"
)

type Report struct {
	Id               uint64    `db:"id"`
	FromUserId       uint64    `db:"from_user"`
	ReportedUserId   uint64    `db:"reported"`
	Reason           string    `db:"reason"`
	RecordedDateTime time.Time `db:"recorded"`
}
