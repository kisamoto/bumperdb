package bumperdb

import (
	p "bitbucket.org/kisamoto/bumperdb/postgres"
	"fmt"
	"log"
)

const (
	userSelect              = "SELECT * FROM %v.%v WHERE Id = %v;"
	searchPreferencesSelect = "SELECT * FROM %v.%v WHERE user_id = %v;"
	userProfileSelect       = "SELECT * FROM %v.%v WHERE user_id = %v;"
)

func FormatQuery(baseQuery, schema, tablePrefix, tableName string) string {
	return fmt.Sprintf(baseQuery, schema, tablePrefix, tableName)
}

func (b *BumperDB) SetupTables() (err error) {
	var tableQuery string

	tableQuery = p.CreateSchema(b.pUserSchema)
	log.Printf("Executing: %v", tableQuery)
	_, err = b.postgresDb.Exec(tableQuery)
	if err != nil {
		return
	}

	tableQuery = p.CreateSchema(b.pAnalyticsSchema)
	log.Printf("Executing: %v", tableQuery)
	_, err = b.postgresDb.Exec(tableQuery)
	if err != nil {
		return
	}

	// Use an array to preserve order
	userTableRows := [][2]string{
		[2]string{tableUser, p.InfoTableRows},
		[2]string{tableUserProfile, p.ProfileTableRows},
		[2]string{tableSearchPreferences, p.SearchPrefRows},
		[2]string{tableLocations, p.LocationRows},
		[2]string{tableLikes, p.LikeRows},
		[2]string{tableBumps, p.BumpRows},
		[2]string{tableBlocks, p.BlockRows},
		[2]string{tableReports, p.ReportRows}}

	analyticsTableRows := [][2]string{
		[2]string{tableVisits, p.AnalyticsVisitsRows},
		[2]string{tablePlaces, p.AnalyticsPlacesRows},
		[2]string{tablePlaceVisits, p.AnalyticsPlaceVisitsRows},
		[2]string{tablePlaceProfile, p.AnalyticsPlaceProfileRows},
		[2]string{tableDistances, p.AnalyticsDistanceRows}}
	// Create all user schema tables
	for _, tableRows := range userTableRows {
		tableQuery = p.CreateTable(b.pUserSchema, tableRows[0], tableRows[1])
		log.Printf("Executing: %v", tableQuery)
		_, err = b.postgresDb.Exec(tableQuery)
		if err != nil {
			return
		}
	}
	// Create all analytics schema tables
	for _, tableRows := range analyticsTableRows {
		tableQuery = p.CreateTable(b.pAnalyticsSchema, tableRows[0], tableRows[1])
		log.Printf("Executing: %v", tableQuery)
		_, err = b.postgresDb.Exec(tableQuery)
		if err != nil {
			return
		}
	}
	return
}

func (b *BumperDB) FormatUserQuery(baseQuery, tableName string) string {
	return FormatQuery(baseQuery, b.pUserSchema, "", tableName)
}

func (b *BumperDB) FormatAnalyticsQuery(baseQuery, tableName string) string {
	return FormatQuery(baseQuery, b.pAnalyticsSchema, "", tableName)
}

func (b *BumperDB) AddLocationPoint(point Location) (err error) {
	return b.dbmap.Insert(&point)
}

func (b *BumperDB) AddLocationPoints(points []Location) (err error) {
	locPointers := make([]interface{}, len(points))
	// Convert into pointers for better use with Gorp
	for i, p := range points {
		locPointers[i] = interface{}(&p)
	}
	return b.dbmap.Insert(locPointers...)
}

func (b *BumperDB) UpsertUserInfo(user User) {
	_, err := b.LookupUser(user.Id)
	if err != nil {
		// We have a user with this Id already, so lets update
		_, err = b.dbmap.Update(&user)
		PanicError(err)
	} else {
		err = b.dbmap.Insert(&user)
		PanicError(err)
	}
}

func (b *BumperDB) UpsertUserProfile(up UserProfile) {
	_, err := b.LookupUserProfile(up.UserId)
	if err != nil {
		_, err = b.dbmap.Update(&up)
		PanicError(err)
	} else {
		err = b.dbmap.Insert(&up)
		PanicError(err)
	}
}

func (b *BumperDB) UpsertUserSearchPrefs(sp SearchPreferences) {
	_, err := b.LookupSearchPrefs(sp.UserId)
	if err != nil {
		_, err = b.dbmap.Update(&sp)
		PanicError(err)
	} else {
		err = b.dbmap.Insert(&sp)
		PanicError(err)
	}
}

func (b *BumperDB) LookupUser(uid uint64) (u User, err error) {
	query := fmt.Sprintf(userSelect, b.pUserSchema, tableUser, uid)
	err = b.dbmap.SelectOne(&u, query)
	return
}

func (b *BumperDB) LookupSearchPrefs(uid uint64) (searchPrefs SearchPreferences, err error) {
	query := fmt.Sprintf(searchPreferencesSelect, b.pUserSchema, tableSearchPreferences, uid)
	err = b.dbmap.SelectOne(&searchPrefs, query)
	return
}

func (b *BumperDB) LookupUserProfile(uid uint64) (up UserProfile, err error) {
	query := fmt.Sprintf(userProfileSelect, b.pUserSchema, tableUserProfile, uid)
	err = b.dbmap.SelectOne(&up, query)
	return
}

func (b *BumperDB) AddLike(like Like) {
	err := b.dbmap.Insert(&like)
	PanicError(err)
}

func (b *BumperDB) AddBump(bump Bump) {
	err := b.dbmap.Insert(&bump)
	PanicError(err)
}

func (b *BumperDB) AddBlock(block Block) {
	err := b.dbmap.Insert(&block)
	PanicError(err)
}
