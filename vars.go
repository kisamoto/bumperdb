package bumperdb

var (
	errNotBumped         = "User '%v' has not bumped into '%v'"
	errNotLikedEachOther = "User '%v' has not liked '%v'"
	errBlocked           = "User '%v' is blocked by '%v'"
)
