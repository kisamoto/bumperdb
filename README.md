# Bumper DB

## Layer

Reusable database operations

## Redis

Redis queuing library to push and pull messages

## PostgreSQL

PostgreSQL connection and database to store a copy of the location
data and perform analytics

* Bumps
  * DateTime
  * Location Point
* Likes
