package bumperdb

import (
	"time"
)

type Message struct {
	Id           uint64    `db:"id"`
	FromUserId   uint64    `json:"from" db:"from"`
	ToUserId     uint64    `json:"to" db:"to"`
	SentDateTime time.Time `json:"sent" db:"sent_datetime"`
	Content      string    `json:"content" db:"message"`
	Received     bool      `json:"received" db:"received_datetime"`
	Seen         bool      `json:"seen" db:"seen"`
}

func (db *BumperDB) SaveMessage(m Message) {
	return
}

func (db *BumperDB) AllMessages(userID string) (messages [][]Message) {
	// Return an array of message arrays
	// Shouldn't really be used
	return
}

func (db *BumperDB) MessagesBetween(userID1, userID2 string, olderThan time.Time, limit int) (messages []Message) {
	// Return up to `limit` messages sent before `olderThan` between 2 users
	return
}
